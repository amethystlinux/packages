# git at it's finest.

rm -rf *.db *.files *.tar.gz *.old *.sig *.bkp

echo [*] Rebuilding repository
repo-add amethyst.db.tar.gz *.tar.xz

rm amethyst.db
rm amethyst.files
mv amethyst.db.tar.gz amethyst.db
mv amethyst.files.tar.gz amethyst.files

git add .
git stage .
git commit -m "$(date)"
git push